/*
    author: Harbinger_of_rain
 */

package net.fryzen.unpacker;

import net.fryzen.unpacker.console.FileOperator;
import net.fryzen.unpacker.console.logger.ErrorLogger;
import net.fryzen.unpacker.console.logger.Logger;
import net.fryzen.unpacker.console.ConsoleView;
import net.fryzen.unpacker.console.Unpacker;

public class Main {
    public static void main(String[] args) {
        Logger fileLogger = new ErrorLogger();
        Logger unpackLogger = new ErrorLogger();
        FileOperator fileOperator = new FileOperator(fileLogger);

        ConsoleView consoleView = new ConsoleView();
        Unpacker unpacker = new Unpacker(fileOperator, unpackLogger);

        unpacker.addObserver(consoleView);

        unpacker.run();

        consoleView.finish();

        consoleView.printErrors("Unpacker", unpackLogger);
        consoleView.printErrors("Files", fileLogger);
    }
}
