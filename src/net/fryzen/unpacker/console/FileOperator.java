/*
    author: Harbinger_of_rain
 */

package net.fryzen.unpacker.console;

import net.fryzen.unpacker.console.logger.Logger;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.file.Files.copy;

public class FileOperator {
    Logger errLogger;

    public FileOperator(Logger errLogger) {
        this.errLogger = errLogger;
    }

    public Logger getErrLogger() {
        return errLogger;
    }

    public boolean createDir(Path path) {
        try {
            Files.createDirectory(path);
            return true;
        } catch (IOException e) {
        }
        return false;
    }

    public boolean copyFile(Path file, Path to) {
        try {
            copy(file, to);
            return true;
        } catch (FileAlreadyExistsException e) {
            errLogger.add("File  " + file + " already exists");
        } catch (IOException e) {
            errLogger.add("copy error on " + file + " to " + to);
        }
        return false;
    }

    public boolean copyDir(File file, File to) {
        try {
            FileUtils.copyDirectory(file, to);
            return true;
        } catch (FileAlreadyExistsException e) {
            errLogger.add("File  " + e + " already exists");
        } catch (IOException e) {
            errLogger.add(e.toString());
        }
        return false;
    }

    public boolean moveDir(Path dir, Path to) {
        try {
            Files.move(dir, to);
        } catch (FileAlreadyExistsException e) {
            errLogger.add("File  " + e + " already exists");
        } catch (IOException e) {
            errLogger.add(e.toString());
        }
        return false;
    }

    public boolean deleteIfEmpty(File file) {
        if (file.listFiles().length == 0) {
            try {
                Files.delete(file.toPath());
                return true;
            } catch (IOException e) {
                errLogger.add(e.toString());
            }
        }
        return false;
    }

    public boolean deleteFile(Path file) {
        if (Files.exists(file)) {
            try {
                Files.delete(file);
                return true;
            } catch (IOException e) {
                errLogger.add(e.toString());
            }
        }
        return false;
    }

    public boolean deleteDir(File dir) {
        if (Files.exists(dir.toPath())) {
            try {
                FileUtils.deleteDirectory(dir);
                return true;
            } catch (IOException e) {
                errLogger.add(e.toString());
            }
        }
        return false;
    }

    public static File[] listSubDirs(Path dir) {
        return new File(dir.toString()).listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.isDirectory();
            }
        });
    }


    public String getName(File file) {
        return new File(file.toString()).getName();
    }
}
