/*
    author: Harbinger_of_rain
 */

package net.fryzen.unpacker.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Executor {
    public List<String> execute(String unpacker, String pakPath, String output) {
        List<String> errors = new ArrayList<>();
        ProcessBuilder pb = new ProcessBuilder(unpacker, pakPath, output);
        BufferedReader errorReader = null;
        Process proc;
        try {
            proc = pb.start();

            errorReader = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
            String s = null;
            while ((s = errorReader.readLine()) != null)
                errors.add(s);

        } catch (IOException e) {
            errors.add(e.toString());
        }
        return errors;
    }
}
